package guy.droid.im.texttovoice;

import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.Locale;

public class MainActivity extends AppCompatActivity implements TextToSpeech.OnInitListener{
    private TextToSpeech tts;
    private Button btnSpeak;
    private EditText txtText;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tts = new TextToSpeech(this, this);
        txtText =(EditText)findViewById(R.id.editText);
        btnSpeak = (Button)findViewById(R.id.button);
        btnSpeak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
speakOut();
            }
        });
    }

    @Override
    public void onInit(int status) {
      /*  if (status == TextToSpeech.SUCCESS) {

            int result = tts.setLanguage(Locale.US);

            if (result == TextToSpeech.LANG_MISSING_DATA
                    || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Toast.makeText(getApplicationContext(),"LANGUAGE NOT SUPPORTED",Toast.LENGTH_LONG).show();
                Log.e("TTS", "This Language is not supported");
            } else {
                Toast.makeText(getApplicationContext(),"SUCCESS",Toast.LENGTH_LONG).show();
            }

        } else {
            Log.e("TTS", "Initilization Failed!");
        } */
    }
    private void speakOut() {

        String text = txtText.getText().toString();

        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null);
    }

}
